using Script.Events;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 捡起道具
/// </summary>
public class PickUpProps : MonoBehaviour
{
    /// <summary>
    /// 物体触发器
    /// 在触碰/撞击时出发
    /// 发起者：Player
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            ExecuteEvents.Execute<IPickUpPropsEvent>(other.gameObject, null,
                (x, y) => x.PickUpProp(transform.gameObject));
        }
    }
}