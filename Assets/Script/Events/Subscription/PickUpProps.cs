﻿using Script.Extension.ScoringSystem;
using UnityEngine;

namespace Script.Events.Subscription
{
    /// <summary>
    /// 拾取道具
    /// </summary>
    public class PickUpProps : MonoBehaviour, IPickUpPropsEvent
    {
        public void PickUpProp(GameObject props)
        {
            if (props.CompareTag("Finish"))
            {
                LocalScorers.Instance.AddScore(1);
            }
        }
    }
}