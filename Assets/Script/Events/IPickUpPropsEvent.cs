﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Script.Events
{
    /// <summary>
    /// 物体触发器事件
    /// </summary>
    public interface IPickUpPropsEvent : IEventSystemHandler
    {
        /// <summary>
        /// 捡起道具
        /// </summary>
        /// <param name="props">道具</param>
        void PickUpProp(GameObject props);
    }
}