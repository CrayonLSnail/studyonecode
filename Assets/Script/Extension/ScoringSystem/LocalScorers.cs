﻿using System;

namespace Script.Extension.ScoringSystem
{
    /// <summary>
    /// 本地计分器
    /// </summary>
    public class LocalScorers
    {
        /// <summary>
        /// 禁用
        /// </summary>
        private LocalScorers()
        {
            SetScoreLock = new object();
        }

        /// <summary>
        /// 懒加载单例
        /// </summary>
        private static readonly Lazy<LocalScorers> InstanceLock = new Lazy<LocalScorers>(() => new LocalScorers());

        #region Propertys

        /// <summary>
        /// 写分数锁
        /// </summary>
        private object SetScoreLock { get; }

        /// <summary>
        /// 分数
        /// </summary>
        private int Score { get; set; } = 0;

        #endregion

        #region Methods

        /// <summary>
        /// 获取计分器
        /// </summary>
        public static LocalScorers Instance => InstanceLock.Value;

        /// <summary>
        /// 添加分数
        /// </summary>
        public void AddScore(int score)
        {
            lock (SetScoreLock)
            {
                this.Score += score;
            }
        }

        public int GetScore()
        {
            return this.Score;
        }

        #endregion
    }
}