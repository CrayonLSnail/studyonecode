using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 跟随
/// </summary>
public class FollowController : MonoBehaviour
{
    /// <summary>
    /// 主角
    /// </summary>
    [SerializeField] private Transform main;

    /// <summary>
    /// 偏移量
    /// </summary>
    private Vector3 _offset;

    // Start is called before the first frame update
    void Start()
    {
        _offset = transform.position - main.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = main.position + _offset;
    }
}