using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    /// <summary>
    /// 目标对象
    /// </summary>
    public Rigidbody body;

    void Start()
    {
        if (body == null)
        {
            body = GetComponent<Rigidbody>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        body.AddForce(Input.GetAxis("Horizontal"), Input.GetAxis("Jump") * 2, Input.GetAxis("Vertical"));
    }
}