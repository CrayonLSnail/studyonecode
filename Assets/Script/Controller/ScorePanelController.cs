﻿using System;
using Script.Extension.ScoringSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Controller
{
    /// <summary>
    /// 计分板控制器
    /// </summary>
    public class ScorePanelController : MonoBehaviour
    {
        private Text _panel;

        private void Start()
        {
            _panel = GetComponent<Text>();
        }

        private void Update()
        {
            _panel.text = $"分数：{LocalScorers.Instance.GetScore()}";
        }
    }
}